### 2020-0510从码云上克隆下学习c语言编程

Chatroom
========

A LAN chat room base on UDP.

Dependences:
------------
- IUP - Portable User Interface

Build
-----
```
cd chatroom
make
```

Screenshots
-----------
![1](screenshots/1.png)
